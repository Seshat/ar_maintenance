My story
---------

- **2002-2006**: Thèse à l'Université de Lyon 
- **2006-2007**: Post-doc au CIMIT à Boston
- **2007-2009**: Post-doc à Imperial College à Londres
- **Depuis 2009**: Maître de conférence à l’UL

- Expertise: **Réalité augmentée et virtuelle** en Médecine

|   |   |
:-------------------------:|:-------------------------:
![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/Intro/simulator.jpg)  | ![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/Intro/anim2.gif)<!-- .element: style="width: 40%;" -->



Recherches
--------
- Modélisation de la respiration
- Simulation de valves cardiaques
- Déformations et mouvements d'organes

|   |   |
:-------------------------:|:-------------------------:
![](https://members.loria.fr/PFVillard/files/results/model_respi.gif)  | ![](https://team.inria.fr/curative/files/2011/07/sofa.gif)<!-- .element: style="width: 50%;" -->



Réalité augmentée et virtuelle
--------
- Qu'est-ce que la réalité virtuelle ?
- Qu'est-ce que la réalité augmentée ?<!-- .element: class="fragment" data-fragment-index="1" -->
- Quelle est la différence entre les deux ?<!-- .element: class="fragment" data-fragment-index="2" -->

![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/Intro/difference.jpg)<!-- .element: style="width: 50%;" -->	



Réalité augmentée et virtuelle
--------

|   |   |   |
----|---|---|
**Réalité virtuelle** | **Réalité augmentée** |   |
 |  |  |
<input type="checkbox"  id="a1"/><label for="a1"><span></span>  </label> | <input type="checkbox"  id="a2"/><label for="a2"><span></span>  </label> |  Le plus immersif  | 
<input type="checkbox"  id="b1"/><label for="b1"><span></span>  </label> | <input type="checkbox"  id="b2"/><label for="b2"><span></span>  </label> |  Enrichit le plus l’expérience utilisateur  | 
 <input type="checkbox"  id="c1"/><label for="c1"><span></span>  </label> | <input type="checkbox"  id="c2"/><label for="c2"><span></span>  </label> |  Utilise un dispositif sous forme de casque |
 <input type="checkbox"  id="d1"/><label for="d1"><span></span>  </label> | <input type="checkbox"  id="d2"/><label for="d2"><span></span>  </label> |  Ne coupe pas l’utilisateur de la réalité  |



Réalité augmentée et maintenance industrielle
--------  

- Effets potentiellement bénéfiques de l’utilisation dans l’industrie
-  **&rarr;** briques technologiques principales de l’[Usine du Futur](https://fr.wikipedia.org/wiki/Industrie_4.0#Réalité_augmentée)
- Applications possibles dans l’industrie nombreuses : 
  - aide à la fabrication, 
  - aide à la maintenance, 
  - documentation, 
  - formation, 
  - sécurité, 
  - etc. 



 Un ouvrier peut ainsi suivre en temps réel les étapes de fabrication d’un produit sans avoir à consulter le manuel correspondant

![alt text](Images_RA/Daqri/daqri_ar9.png) 



un rondier pourrait avoir facilement accès aux valeurs nominales des capteurs qu’il inspecte

![alt text](Images_RA/Daqri/daqri_ar7.jpg)



un formateur pourrait guider à distance son apprenti

![alt text](Images_RA/Microsoft/MS_RA4.jpg)



- Exemple 1 : **Daqri**

|   |   |
:-------------------------:|:-------------------------:
![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/Intro/daqri_glasses.png) | ![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/Intro/daqri.gif)<!-- .element: style="width: 8	0%;" -->



Réalité augmentée et maintenance industrielle
--------  

- Exemple 2 : **Edusafe** au CERN

![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/Intro/radioactivity.jpg)

&rarr; Niveaux de radioactivité



Réalité augmentée et maintenance industrielle
--------  

- Exemple 3 : **iAR**

|   |   |
:-------------------------:|:-------------------------:
![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/Intro/iAR_ar1.jpg) | ![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/Intro/iAR.gif)<!-- .element: style="width: 10%;" -->



Réalité augmentée et maintenance industrielle
--------  

- Exemple 4 : **[Hololens](https://youtu.be/d3YT8j0yYl0?t=60)**

![](Images_RA/Microsoft/anim.gif)



- Exemple 4 : **Magic Leap**
  
![](https://ml-cms.imgix.net/2pfOq3nfvt1kqONFdz4YH9/fc6cadd85539c72ca5a5eaa310b46794/manifest_thumb__3x.jpg?auto=format%2Ccompress)<!-- .element: style="width: 80%;" -->