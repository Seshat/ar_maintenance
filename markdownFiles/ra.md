# Introduction technique à la réalité augmentée

[PF Villard](http://members.loria.fr/PFVillard/)



## Plan 

1. Introduction 
+ Réalité augmentée basée **capteur**
+ Réalité augmentée basée **vision** 
+ Réalité augmentée avec **Unity**



## Introduction
- Compléter notre perception du **monde réel** avec des **éléments fictifs**, non perceptibles naturellement

- Le plus souvent : incruster de façon réaliste des **objets virtuels 3D** dans une **séquence d’images**

- Pour les puristes : interactif, **temps réel**



## Difficultés
<div style="position:absolute;  top:100pt;left:100pt;height:70%; margin:0 auto;">
<img data-src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra1.png" >
</div>
<br><br><br><br>

- Perspective <!-- .element: class="fragment" data-fragment-index="1" -->
- Forme <!-- .element: class="fragment" data-fragment-index="2" -->
- Lumière <!-- .element: class="fragment" data-fragment-index="3" -->
<br><br><br><br><br><br><br><br>



## Exemple
<div style="  width:640px; height:480px;   margin:0 auto;">
  <img  src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra2a.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra2b.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra2c.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra2d.png" style="position:absolute;top:0;left:0;" />      
</div>



## Perspective projection
<div style=" position:absolute;  width:640px; height:480px;   margin:0 auto;">
  <img  src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra3a.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra3b.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra3c.png" style="position:absolute;top:0;left:0;" />   
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra3d.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra3e.png" style="position:absolute;top:0;left:0;" /> 
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra3f.png" style="position:absolute;top:0;left:0;" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra3g.png" style="position:absolute;top:0;left:0;" />    
</div><br><br><br><br><br>

- Le modèle Sténopé :
	- plan <font  color="green">P</font> (grille du dessin)
	- point 3D <font  color="blue">M</font> de la scène projeté en <font  color="black">m</font> sur <font  color="green">P</font>
	- centre de projection <font  color="red">C</font> (viseur)



## Inversion de perspective
- Retrouver le point de vue des yeux en temps réel
- Différentes méthodes
  - <b>Capteurs</b> : robustes, peu précis
  - <b>Analyse d’image</b> : précis, peu robuste
  - <b>Solutions Hybrides</b> : précises et robustes



## Techniques basées capteurs



## Capteurs magnétiques 
3 bobines orthogonales émettent un champ magnétique capté par 3 bobines réceptives
<div>
- **+** Bonne précision (0.2 mm, 0.1 deg)
- **-** Sensible aux perturbations du champ magnétique
- **-** Environnement de taille réduite (3m × 3m × 3m)
</div> <!-- .element: style="float: left; width: 60%;" -->
<img data-src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra4.png" >



## Capteurs acoustiques
3 microphones / haut-parleurs, intersection de sphères
<div>
- **+** Bonne précision (≅ magnétique)
- **+** Non sensible aux objets métalliques
- **-** Environnement de taille réduite (3m × 3m × 3m)
</div> <!-- .element: style="float: left; width: 60%;" -->
<img data-src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra5.png" >



## Capteurs optiques 
Caméras stéréo + cibles + triangulation
<div>
- **+** Très précis (0.1 mm)
- **-** Volume restreint
- **-** Risque d’occultation des cibles
</div> <!-- .element: style="float: left; width: 60%;" -->
<img data-src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra6.png" >



## Capteurs inertiels 
Fusion de données provenant de capteurs “autonomes” (gyroscopes, accéléromètres, magnétomètres)
<div>
- **+** Volume non restreint (env. extérieur ok)
- **-** Rotations uniquement
- **-** Peu précis
</div> <!-- .element: style="float: left; width: 60%;" -->
<img data-src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra7.png" >



## GPS	 
Système de navigation par satellite (intersection de disques)
<div>
- **+** Environnement extérieur
- **-** Positions uniquement
- **-** Très peu précis (10-20 m)
</div> <!-- .element: style="float: left; width: 60%;" -->
<img data-src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ra8.gif" >



## Techniques basées algorithmes



Plusieurs techniques basées images existent :
- Utiliser les **points de fuite** (Sketchup…)
- Utiliser des **correspondances 3D-2D** de points (3DS Max…)
- **Manuellement**, par tâtonnement (tous les logiciels de modélisation acceptant une image d’arrière-plan)

Seule la 2 permet peut être appliquée de façon automatique dans des séquences vidéos ! <!-- .element: class="fragment" data-fragment-index="1" -->



## La méthode des points de fuites
- Trouver la matrice de projection par point de fuite

|             |   |   |
:-------------------------:|:-------------------------:
![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra9a.png)  |  ![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra9b.png)  |  ![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra9c.png)
One-point perspective projection | 	Frankfurt International Airport | 	Pietro Perugino in Sistine Chapel (1481–82)



## Exemple dans Sketchup
![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra10.png)



Utiliser deux points de fuite correspondant à des droites horizontales de directions orthogonales
<div style=" position:absolute;  width:640px; height:480px;   margin:0 auto;">
<!-- for file in *.png; do convert ${file} -transparent white ${file}; done -->
<!-- convert ptFuite*.png -crop 500x400+250+280 fuite%03d.png -->
  <span class="fragment fade-out" data-fragment-index="2">
    <img src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ptFuite/fuite000.png" style="position:absolute;top:0;left:0;" />  
  </span>
  <span class="fragment fade-out" data-fragment-index="3">
    <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ptFuite/fuite001.png" style="position:absolute;top:0;left:0;" data-fragment-index="1" />
  </span>
  <span class="fragment fade-out" data-fragment-index="4">
    <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ptFuite/fuite002.png" style="position:absolute;top:0;left:0;" data-fragment-index="2" />
  </span>
  <span class="fragment fade-out" data-fragment-index="5">
    <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ptFuite/fuite003.png" style="position:absolute;top:0;left:0;" data-fragment-index="3" />
  </span>
  <span class="fragment fade-out" data-fragment-index="6">
    <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ptFuite/fuite004.png" style="position:absolute;top:0;left:0;" data-fragment-index="4" />
  </span>
  <span class="fragment fade-out" data-fragment-index="7">
    <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ptFuite/fuite005.png" style="position:absolute;top:0;left:0;" data-fragment-index="5" />
  </span>
  <span class="fragment fade-out" data-fragment-index="8">
    <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ptFuite/fuite006.png" style="position:absolute;top:0;left:0;" data-fragment-index="6" />
  </span>
    <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/ptFuite/fuite007.png" style="position:absolute;top:0;left:0;" data-fragment-index="7" />
</div>



## Exemple dans Sketchup
<div style="  width:1200px; height:900px;   margin:0 auto;">
  <img src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/sketchup/sketchup.001.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/sketchup/sketchup.003.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/sketchup/sketchup.004.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/sketchup/sketchup.005.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/sketchup/sketchup.006.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/sketchup/sketchup.007.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/sketchup/sketchup.008.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/sketchup/sketchup.009.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/sketchup/sketchup.010.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/sketchup/sketchup.011.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/sketchup/sketchup.012.jpeg" style="position:absolute;top:200;left:0;width:800px;height:500px" />
</div>



## La méthode de l’inversion de perspectives
Trouver la matrice de projection par connaissance des points dans le monde réel

![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra11.png)



## Système d'équations

Inversion de la caméra = calcul de R, t :
- focale connue : 4 points suffisent
- focale non connue : 6 points

Utilisation de correspondances 3D-2D 

<div style="  position:absolute;  width:640px; height:480px;    margin:0 auto;">
<!-- for file in *.png; do convert ${file} -transparent white ${file}; done -->
<!-- convert inv*.png -crop 525x300+255+170 inverse%03d.png -->
  <img src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/inv/inverse000.png" style="position:absolute;top:0;left:0;" />
  <span class="fragment fade-out"  data-fragment-index="2">
  	<img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/inv/inverse001.png" style="position:absolute;top:0;left:0;" data-fragment-index="1"/>
  </span>
  <span class="fragment fade-out" data-fragment-index="3">
  	<img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/inv/inverse002.png" style="position:absolute;top:0;left:0;" data-fragment-index="2" />  
  </span>
  <span class="fragment fade-out"  data-fragment-index="4">
  	<img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/inv/inverse003.png" style="position:absolute;top:0;left:0;" data-fragment-index="3" />
  </span>
  <span class="fragment fade-out"  data-fragment-index="5">	
  	<img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/inv/inverse004.png" style="position:absolute;top:0;left:0;" data-fragment-index="4" />
  </span>
  <span class="fragment fade-out"  data-fragment-index="6">
  	<img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/inv/inverse005.png" style="position:absolute;top:0;left:0;" data-fragment-index="5" />
  </span>
  <span class="fragment fade-out"  data-fragment-index="7">
  	<img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/inv/inverse006.png" style="position:absolute;top:0;left:0;" data-fragment-index="6" />
  </span>
  <span class="fragment fade-out"  data-fragment-index="8">
  	<img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/inv/inverse007.png" style="position:absolute;top:0;left:0;" data-fragment-index="7" />
  </span>
  <span class="fragment fade-out"  data-fragment-index="9">
  	<img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/inv/inverse008.png" style="position:absolute;top:0;left:0;" data-fragment-index="8" />
  </span>
  <span class="fragment fade-out"  data-fragment-index="10">
  	<img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/inv/inverse009.png" style="position:absolute;top:0;left:0;" data-fragment-index="9" />
  </span>
  <img class="fragment" src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files//RA/inv/inverse010.png" style="position:absolute;top:0;left:0;" data-fragment-index="10" />
</div>



## Calcul à la main

|   |   |
:-------------------------:|:-------------------------:
![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra12a.png)  | ![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra12d.png)
![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra12b.png) | 	![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra12c.png) 



![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra13.png)

![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra14.jpg)<!-- .element: style="width: 60%;" -->



## Ex : projets étudiants
|   |   |
:-------------------------:|:-------------------------:
![](https://homepages.loria.fr/PFVillard/polar/img/animation.gif)<!-- .element: style="width: 77%;" -->  |  ![](https://homepages.loria.fr/PFVillard/polar/img/YiLu.gif)<!-- .element: style="width: 70%;" -->

avec <a href="http://polar.inria.fr"><strong><span style="color: #33cccc">PoLAR</span></strong></a>  <img src="http://project.inria.fr/polar/files/2016/10/dessin6.png" alt="PoLAR" width="60" height="50" style="border:0;">



## Calcul automatique par marqueurs fiduciaux
- **Fiduciaux** = détectables et identifiables
- Comporte 4 étapes principales :
	1. Extraction des **points d’intérêt** dans chaque image
	+ **Mise en correspondance** robuste des points d’intérêt entre images voisines
	+ **Reconstruction projective** des points d’intérêt appariés
	+ **Reconstruction métrique** (autocalibrage)
- Exemple : [ARToolkit](https://hitl.washington.edu/artoolkit/)



## Algorithme d'ARToolkit

![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra15.png)



## Applications



### Réalité augmentée et maintenance industrielle
--------  

|   |   |
:-------------------------:|:-------------------------:
![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/Intro/daqri_glasses.png) | ![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/Intro/daqri.gif)<!-- .element: style="width: 80%;" -->



### Système de navigation sur smartphone
--------

![alt text](Images_RA/Google/google-maps-ar.jpg) 



### Aide à l’intervention chirurgicale
--------

![alt text](Images_RA/Novarad/hololens-chirurgie.jpg) 



### Jeux vidéo
--------

<video controls autoplay loop>
  <source src="https://seshat.gitlabpages.inria.fr/unity/img/minecraft.mp4" type="video/mp4">
</video>

<video controls autoplay loop>
  <source src="https://seshat.gitlabpages.inria.fr/unity/img/pokemonGo.mp4" type="video/mp4">
</video>



### Films
--------

<div style="height:300px">![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/film1.jpg) </div>
<div style="height:300px">![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/film2.gif) </div>



### Culture
--------

<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/culture1.jpg) </div>
<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/culture2.gif) </div>



### Architecture
--------

<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/archi.gif) </div>
<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/archi2.jpg) </div>



### Archéologie
--------

<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/archeo1.jpg) </div>
<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/archeo2.jpg) </div>



### eCommerce
--------

<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/ecom2.png) </div>
<div style="height:300px"><iframe width="560" height="315" src="https://www.youtube.com/embed/vDNzTasuYEw?start=20" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </div>



### Publicité

<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/pub1.gif) </div>
<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/pub2.jpg)  </div>



### Sport
--------

<div style="height:200px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/sport1.png) ![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/sport2.jpg)  </div>
<div style="height:200px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/sport3.jpg) ![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/sport4.jpg)  </div>



### Education
--------

<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/edu1.gif) </div>
<div style="height:300px">![alt text](https://gitlab.inria.fr/Seshat/unity/-/raw/master/img/edu2.jpg)  </div>



## Réalité augmentée avec Unity



## Solution dans Unity: Vuforia
- Plugin développé par **Qualcomm** pour
	- Gestion de la caméra
	- Détection de l’environnement
	- Affichage d'un objet 3D projeté

![](https://www.avrspot.com/wp-content/uploads/2018/06/vuforia-logo.png)<!-- .element: style="width: 34%;" -->

- Créer un compte sur [Vuforia](https://developer.vuforia.com) et se connecter
- Créer une licence dans le menu **License Manager**



## Workflow d'utilisation du plugin Vuforia
1. Créer un compte sur le site de Vuforia
+ **Télécharger** et **installer** le plugin
+ **Définir des images** à interpréter comme marqueurs
+ Constituer la **base de données** pour Unity (*Target Manager*)
+ **Importer** cette base de données dans Unity

Les fichiers images correspondants aux marqueurs utilisés en classe se trouvent [ici](../webGL_files/RA/Images_Vuforia.zip)



## Utilisation du Target Manager
<div>
- Aller dans le menu **Target Manager**
- Ajouter une **Database**
- Sélectionner **Device** (le cloud est payant)
- Définir un nom
- Ajouter une **Target**
- Sélectionner une **Single image**
- Remplir les champs
- **Télécharger** la *Database* correspondante
</div> <!-- .element: style="float: left; width: 65%;" -->

![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra16.png)<!-- .element: style="float: right; width: 34%;" -->



## Qualité du marqueur
![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra17.png)<!-- .element: style="width: 80%;" -->



## Conseil pour une bonne qualité de marqueur
- Avoir un maximum de **détails**
- Avoir un **contraste** élevé
- L'image doit être **assez grande**
- L'image doit être acquise en **position plane**
- L'image doit être acquise en **évitant les reflets**
- Le marqueur doit être **facile à manipuler** (Ex : *cartes à jouer*)



## Intégration dans Unity
<div>
- Si Vuforia n'est pas installé, Aller sur le [site du plugin](https://unity3d.com/get-unity/download), sélectionner Unity et installer Unity avec Vuforia
- Dans un projet Unity déjà créé :
  - Vérifier que Vuforia est bien activé (*Edit*&rarr;*Project settings*&rarr;*Player*)
  - Dans l'onglet *Project*, mettre le numéro de licence dans *Resources*&rarr;*VuforiaConfiguration*
  - Importer votre *Database* (*Asset*&rarr;*Import package*&rarr;*Custom package...*)
</div> <!-- .element: style="float: left; width: 70%;" -->

![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra18.png)<!-- .element: style="width: 29%;float: right;" -->



## Exercice : Création d'une scène avec RA
- Ajouter une **ImageTarget** (*GameObject*&rarr;*Vuforia*&rarr;*Image*)
- Remplacer *camera* par **ARcamera** (*...*&rarr;*ARcamera*)
- Sélectionner un marqueur dans l'*Inspector*
- Ajouter une **sphère** dans *ImageTarget* de l'onglet *Hierachy*

![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra19.png)<!-- .element: style="width: 55%;float: left;" class="fragment" -->

<div>
- &rarr; Tester les **différentes images** avec des **modèles 3D plus compliqués**
- &rarr; Tester avec plusieurs marqueurs en **même temps**
</div><!-- .element: style="width: 44%;float: right;" class="fragment" -->



## Interaction entre marqueurs
- Dans les configurations de Vuforia, permettre le suivi de **2 cibles**
- Exemple (à reproduire) avec deux objets 3D:

![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra21.jpg)<!-- .element: style="width: 40% -->



## Résultat ([ici](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra22V2.mp4))
![](https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra22V2.mp4)<!-- .element: style="width: 1% -->

<video controls autoplay loop>
  <source src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/ra22V2.mp4" type="video/mp4">
</video>




## Exemple de travaux d'étudiants 



## Apprentissage du magnétisme

<video controls autoplay loop>
  <source src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/exemple/magnetisme.mp4" type="video/mp4">
</video>


## Roman interactif

<video controls autoplay loop>
  <source src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/exemple/romanInteractif.mp4" type="video/mp4">
</video>


## Apprentissage de la paléontologie

<video controls autoplay loop>
  <source src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/exemple/paleanthologie.mp4" type="video/mp4">
</video>


## Apprentissage des équations chimiques

<video controls autoplay loop>
  <source src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/exemple/molecules.mp4" type="video/mp4">
</video>


## Apprentissage de la composition d'une fleur

<video controls autoplay loop>
  <source src="https://gitlab.inria.fr/Seshat/unity/-/raw/master/webGL_files/RA/exemple/fleur.mp4" type="video/mp4">
</video>



### En 2022

<video controls autoplay loop>
  <source src="https://seshat.gitlabpages.inria.fr/unity/lamap/video22/fleur.mp4" type="video/mp4">
</video>



### Apprentissage du système solaire <!-- omit in toc -->

<!-- ![](webGL_files/RA/exemple/fleur.mp4)<!-- .element: width=50% -->
<video controls autoplay loop>
  <source src="https://seshat.gitlabpages.inria.fr/unity/webGL_files/RA/exemple/solaire.mp4" type="video/mp4">
</video>


### En 2022

<video controls autoplay loop>
  <source src="https://seshat.gitlabpages.inria.fr/unity/lamap/video22/planet.mp4" type="video/mp4">
</video>



### Apprentissage de types d’éruptions volcaniques <!-- omit in toc -->

<video controls autoplay loop>
  <source src="https://seshat.gitlabpages.inria.fr/unity/webGL_files/RA/exemple/volcan.mp4" type="video/mp4">
</video>



### En 2022

<video controls autoplay loop>
  <source src="https://seshat.gitlabpages.inria.fr/unity/lamap/video22/volcan.mp4" type="video/mp4">
</video>



### Apprentissage de langue <!-- omit in toc -->

<video controls autoplay loop>
  <source src="https://seshat.gitlabpages.inria.fr/unity/lamap/video22/langue.mp4" type="video/mp4">
</video>



### Apprentissage des séismes <!-- omit in toc -->

<video controls autoplay loop>
  <source src="https://seshat.gitlabpages.inria.fr/unity/lamap/video22/seisme.mp4" type="video/mp4">
</video>


